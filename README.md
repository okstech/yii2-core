Yii2 Core
=========
Core

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oks/yii2-core "*"
```

or add

```
"oks/yii2-core": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \oks\core\AutoloadExample::widget(); ?>```